---
title: "CMS Support Plan"
description: "Documentation on how to request support for issues related to the CMS"
---

## During Digital Experience Team Working Hours

**Point person:** [Nathan Dubord](https://gitlab.enterprise.slack.com/archives/D021YDB4FM4)

Eastern Timezone (UTC−5)

Working hours: 9am - 6pm Eastern

1. The DEX team is spread across North American time zones. Regular working hours span from 6am - 5:30pm Pacific (Convert to local timezone [here](https://www.timeanddate.com/worldclock/converter.html)).
2. If you experience any issues related to our CMS that require immediate attention, please post in [#digital-experience-team](https://gitlab.enterprise.slack.com/archives/CN8AVSFEY)
    1. Please tag the following people:
        2. [Lauren Barker](https://gitlab.enterprise.slack.com/archives/D0168EQ62EP)
        3. [Nathan Dubord](https://gitlab.enterprise.slack.com/archives/D021YDB4FM4)
        4. [Megan Filo](https://gitlab.enterprise.slack.com/archives/D02SNEUHZ3L)
        5. [Laura Duggan](https://gitlab.enterprise.slack.com/archives/D01H18BBUTW)
3. You can expect a response within minutes.
4. All CMS related issues will be an immediate priority.

## Outside Of Digital Experience Team Working Hours

**Point person:** [Nathan Dubord](https://gitlab.enterprise.slack.com/archives/D021YDB4FM4)

Eastern Timezone (UTC−5)

Working hours: 9am - 6pm Eastern

1. The DEX team is spread across North American time zones. Regular working hours span from 6am - 5:30pm Pacific (Convert to local timezone [here](https://www.timeanddate.com/worldclock/converter.html)).
2. If your issue occurs +/- 2hrs of our working hours, please text (phone numbers are available in Slack profiles) the following people based on timezone
    1. Eastern Timezone (UTC−5):
        1. [Nathan Dubord](https://gitlab.enterprise.slack.com/archives/D021YDB4FM4)
        2. [Laura Duggan](https://gitlab.enterprise.slack.com/archives/D01H18BBUTW)
    2. Central Timezone (UTC−6):
        1. [Megan Filo](https://gitlab.enterprise.slack.com/archives/D02SNEUHZ3L)
    3. Pacific Timezone (UTC−8):
        2. [Lauren Barker](https://gitlab.enterprise.slack.com/archives/D0168EQ62EP)
3. If the issue occurs outside of our working hours, please refer to our CMS Troubleshooting Playbook.
    1. GitLab team members who may be able to assist via Slack include:
        1. [Hanif Smith-Watson](https://gitlab.enterprise.slack.com/team/U014T1XSKT6) (UK, UTC +0)
        2. [Gonzalo Servat](https://gitlab.enterprise.slack.com/team/U02QHDGTTD2) (Australia, UTC+10)
        3. [Nick Veenhof](https://gitlab.enterprise.slack.com/team/U03BDC9MVKR) (Belgium, UTC +1)
4. If you have to wait for a Digital Experience team member to come online, you could create an issue using this template that details the problem which will get a headstart: https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/issues/new

## If there is a critical stop business (on the web) error
### Immediately Text
In order (numbers are in Slack profiles):

1. [Lauren Barker](https://gitlab.enterprise.slack.com/archives/D0168EQ62EP)
1. [Carrie Maynard](https://gitlab.enterprise.slack.com/archives/D03RZD1F2JV)

_Call on the phone if no response within 15 minutes_

## CMS Troubleshooting Playbook
For information on troubleshooting a technical issue, refer to this playbook: [CMS Troubleshooting Playbook](/handbook/marketing/digital-experience/incident-response-playbook/)